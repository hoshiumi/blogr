# Blogr landing page

This is a solution to the [Blogr landing page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/blogr-landing-page-EX2RLAApP). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

-   [Overview](#overview)
    -   [The challenge](#the-challenge)
    -   [Screenshot](#screenshot)
    -   [Links](#links)
-   [My process](#my-process)
    -   [Built with](#built-with)
    -   [What I learned](#what-i-learned)
    -   [Continued development](#continued-development)
    -   [Useful resources](#useful-resources)
-   [Author](#author)
-   [Acknowledgments](#acknowledgments)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### The challenge

Users should be able to:

-   View the optimal layout for the site depending on their device's screen size
-   See hover states for all interactive elements on the page

### Screenshot

Here is the design we have to implement :
![](./src/images/desktop-design.jpg)

### Links

-   Solution URL: [Click here](https://www.frontendmentor.io/solutions/blogr-landing-page-in-htmlcss-vanilla-js-S1gqxNXQc)
-   Live Site URL: [Click here](https://gleeful-chimera-7fd159.netlify.app/)

## My process

### Built with

-   Semantic HTML5 markup
-   SCSS
-   Vanilla JavaScript
-   Netlify for Hosting
-   Flexbox
-   [ViteJS](https://vitejs.dev/)
-   Desktop-first workflow

### What I learned

This challenge learned me to better nest my elements because of the navbar that was special to implement on mobile, I had problems with that. Indeed, it is the second full page frontend page I implement and it was quite a challenge compared to the other components implementations.
