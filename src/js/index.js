const navLinks = document.querySelectorAll('.head_navlink')
const nav = document.querySelector('.head_nav')
const iconMenu = document.querySelector('.icon_menu')

navLinks.forEach((navLink) => {
    navLink.addEventListener('click', () => {
        const panel = navLink.nextElementSibling

        if (navLink.classList.contains('active')) {
            navLink.classList.remove('active')
            panel.classList.remove('active')
        } else {
            navLinks.forEach((nav) => {
                nav.classList.remove('active')
                nav.nextElementSibling.classList.remove('active')
            })
            navLink.classList.add('active')
            panel.classList.add('active')
        }
    })
})

iconMenu.addEventListener('click', () => {
    nav.classList.toggle('active')
})
